package demo

class Book {
    String title
    String author
    String about
    String href
    static mapping = {
        about type: 'text'
    }
}