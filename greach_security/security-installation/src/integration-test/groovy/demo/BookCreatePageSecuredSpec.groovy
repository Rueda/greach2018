package demo

import geb.spock.GebSpec
import grails.testing.mixin.integration.Integration

@Integration
class BookCreatePageSecuredSpec extends GebSpec {

    def "book create page is secured"() {
        when:
        via BookCreatePage

        then:
        at LoginPage
    }
}
