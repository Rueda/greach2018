package demo

import geb.spock.GebSpec
import grails.testing.mixin.integration.Integration

@Integration
class BookListPageSecuredSpec extends GebSpec {

    def "book list page is secured"() {
        when:
        via BookListPage

        then:
        at LoginPage
    }
}
