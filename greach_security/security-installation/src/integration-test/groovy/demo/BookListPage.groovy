package demo

import geb.Page

class BookListPage extends Page {

    static url = '/'

    static at = { title == 'Book List' }
}
