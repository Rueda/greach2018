package demo

import geb.spock.GebSpec
import grails.testing.mixin.integration.Integration

@Integration
class BookShowPageSecuredSpec extends GebSpec {

    def "book show page is secured"() {
        when:
        via BookShowPage, 1

        then:
        at LoginPage
    }
}
