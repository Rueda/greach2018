package demo

import geb.spock.GebSpec
import grails.testing.mixin.integration.Integration

@Integration
class BookShowPageSecuredSpec extends GebSpec {
    UserDataService  userDataService

    def "book show page is secured"() {
        given:
        User user = userDataService.save('sherlock', 'elementary')

        when:
        via BookShowPage, 1

        then:
        at LoginPage

        when:
        LoginPage loginPage = browser.page LoginPage
        loginPage.login(user.username, 'elementary')

        then:
        at BookShowPage

        cleanup:
        userDataService.delete(user.id)
    }
}
