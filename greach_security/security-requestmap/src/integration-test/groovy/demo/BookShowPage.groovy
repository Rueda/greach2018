package demo

import geb.Page

class BookShowPage extends Page {

    static url = '/book/show'

    static at = { title == 'Show Book'}

    String convertToPath(Object[] args) {
        args ? "/${args[0]}".toString() : ''
    }
}
