package demo

import geb.spock.GebSpec
import grails.testing.mixin.integration.Integration
import grails.transaction.Rollback

@Integration
class BookCreatePageSecuredSpec extends GebSpec {

    UserDataService userDataService

    def "book create page is secured"() {
        given:
        User user = userDataService.save('sherlock', 'elementary')

        when:
        via BookCreatePage

        then:
        at LoginPage

        when:
        LoginPage loginPage = browser.page LoginPage
        loginPage.login(user.username, 'elementary')

        then:
        at BookCreatePage

        cleanup:
        userDataService.delete(user.id)
    }
}
