package demo

import geb.Page

class LoginPage extends Page {

    static url = '/login/auth'

    static at = { title == 'Login' }

    static content = {
        usernameField { $('#username', 0) }
        passwordField { $('#password', 0) }
        inputSubmit { $('#submit', 0)}
    }

    void login(String username, String password) {
        fillField(usernameField, username)
        fillField(passwordField, password)
        inputSubmit.click()
    }

    void fillField(def field, String value) {
        for ( char c : value.toCharArray() ) {
            field << "${c}".toString()
        }
    }
}
