package demo

import geb.spock.GebSpec
import grails.testing.mixin.integration.Integration

@Integration
class BookControllerIntegrationSpec extends GebSpec {

    UserDataService userDataService
    RoleDataService roleDataService
    UserRoleDataService userRoleDataService

    def "authenticated users without roles are not able to see books"() {
        given:
        User user = userDataService.save('sherlock', 'elementary')

        when:
        via BookListPage

        then:
        at LoginPage

        when:
        LoginPage loginPage = browser.page LoginPage
        loginPage.login(user.username, 'elementary')

        then:
        browser.title == 'Denied'

        cleanup:
        userDataService.delete(user.id)
    }

    def "users with role ROLE_AUTHOR is able to create a a book, view it and deleted"() {
        given:
        Role role = roleDataService.save('ROLE_AUTHOR')
        User user = userDataService.save('sherlock', 'elementary')
        userRoleDataService.save(user, role)

        when:
        via BookListPage

        then:
        at LoginPage

        when:
        LoginPage loginPage = browser.page LoginPage
        loginPage.login(user.username, 'elementary')

        then:
        at BookListPage

        when:
        BookListPage bookListPage = browser.page BookListPage

        then:
        bookListPage.table.numberOfRows() == 0

        when:
        bookListPage.buttons.create()

        then:
        browser.at BookCreatePage

        when:
        BookCreatePage bookCreatePage = browser.page BookCreatePage
        bookCreatePage.title = 'Practical Grails 3'
        bookCreatePage.author = 'Eric Helgeson'
        bookCreatePage.href = 'https://www.grails3book.com/'
        bookCreatePage.about = 'Learn the fundamental concepts behind building Grails applications with the first book dedicated to Grails 3.'

        bookCreatePage.save()

        then:
        browser.at BookShowPage

        when:
        BookShowPage bookShowPage = browser.page BookShowPage

        withConfirm { bookShowPage.buttons.delete() }

        then:
        at BookListPage

        when:
        bookListPage = browser.page BookListPage

        then:
        bookListPage.table.numberOfRows() == 0

        cleanup:
        userRoleDataService.delete(user, role)
        userDataService.delete(user.id)
        roleDataService.delete(role.id)
    }

    def "users with role ROLE_EDITOR is able to create a a book, view it and deleted"() {
        given:
        Role role = roleDataService.save('ROLE_EDITOR')
        User user = userDataService.save('sherlock', 'elementary')
        userRoleDataService.save(user, role)

        when:
        via BookListPage

        then:
        at LoginPage

        when:
        LoginPage loginPage = browser.page LoginPage
        loginPage.login(user.username, 'elementary')

        then:
        at BookListPage

        when:
        BookListPage bookListPage = browser.page BookListPage

        then:
        bookListPage.table.numberOfRows() == 0

        when:
        bookListPage.buttons.create()

        then:
        browser.at BookCreatePage

        when:
        BookCreatePage bookCreatePage = browser.page BookCreatePage
        bookCreatePage.title = 'Practical Grails 3'
        bookCreatePage.author = 'Eric Helgeson'
        bookCreatePage.href = 'https://www.grails3book.com/'
        bookCreatePage.about = 'Learn the fundamental concepts behind building Grails applications with the first book dedicated to Grails 3.'

        bookCreatePage.save()

        then:
        browser.at BookShowPage

        when:
        BookShowPage bookShowPage = browser.page BookShowPage

        withConfirm { bookShowPage.buttons.delete() }

        then:
        at BookListPage

        when:
        bookListPage = browser.page BookListPage

        then:
        bookListPage.table.numberOfRows() == 0

        cleanup:
        userRoleDataService.delete(user, role)
        userDataService.delete(user.id)
        roleDataService.delete(role.id)
    }
}
