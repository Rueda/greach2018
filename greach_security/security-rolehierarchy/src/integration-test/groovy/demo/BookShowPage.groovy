package demo

import geb.Page

class BookShowPage extends Page {

    static url = '/book/show'

    static at = { title == 'Show Book'}

    String convertToPath(Object[] args) {
        args ? "/${args[0]}".toString() : ''
    }

    static content = {
        row { $('li.fieldcontain .property-label', text: it).parent() }
        value { row(it).find('.property-value').text() }
        buttons { $('fieldset.buttons').module(CreateEditDeleteModule) }
    }

    String getName() {
        value('Name')
    }
}
