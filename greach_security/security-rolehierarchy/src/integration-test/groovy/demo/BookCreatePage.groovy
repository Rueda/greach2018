package demo

import geb.Page
import geb.module.TextInput

class BookCreatePage extends Page {

    static url = '/book/create'

    static at = { title == 'Create Book' }

    static content = {
        inputField { $('input', name: it).module(TextInput) }
        saveButton { $('input', type: 'submit') }
    }

    void setTitle(String value) {
        populate('title', value)
    }

    void setHref(String value) {
        populate('href', value)
    }

    void setAbout(String value) {
        populate('about', value)
    }

    void setAuthor(String value) {
        populate('author', value)
    }

    void save() {
        saveButton.click()
    }

    protected void populate(String name, String value) {
        for ( char c : value.toCharArray()) {
            inputField(name) << "${c}".toString()
        }
    }
}
