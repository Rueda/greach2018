package demo

import geb.Page

class BookListPage extends Page {

    static url = '/book/index'

    static at = { title == 'Book List' }

    static content = {
        table { $('div.content', 0).module(TableModule) }
        buttons { $('fieldset.buttons').module(CreateModule) }
        message { $('div.message').text() }
    }
}
