package demo

import grails.gorm.services.Service
import groovy.transform.CompileStatic

@CompileStatic
@Service(Role)
interface RoleDataService {
    Role save(String authority)
    void delete(Serializable id)
}