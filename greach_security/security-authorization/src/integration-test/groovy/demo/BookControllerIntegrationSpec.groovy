package demo

import geb.spock.GebSpec
import grails.testing.mixin.integration.Integration

@Integration
class BookControllerIntegrationSpec extends GebSpec {

    UserDataService userDataService
    RoleDataService roleDataService
    UserRoleDataService userRoleDataService
    BookDataService bookDataService

    def "users with role ROLE_DETECTIVE is able to create a a book, view it and deleted"() {
        given:
        Role role = roleDataService.save('ROLE_DETECTIVE')
        User user = userDataService.save('sherlock', 'elementary')
        userRoleDataService.save(user, role)

        when:
        via BookListPage

        then:
        at LoginPage

        when:
        LoginPage loginPage = browser.page LoginPage
        loginPage.login(user.username, 'elementary')

        then:
        at BookListPage

        when:
        BookListPage bookListPage = browser.page BookListPage

        then:
        bookListPage.table.numberOfRows() == 0

        when:
        bookListPage.buttons.create()

        then:
        browser.at BookCreatePage

        when:
        BookCreatePage bookCreatePage = browser.page BookCreatePage
        bookCreatePage.title = 'Practical Grails 3'
        bookCreatePage.author = 'Eric Helgeson'
        bookCreatePage.href = 'https://www.grails3book.com/'
        bookCreatePage.about = 'Learn the fundamental concepts behind building Grails applications with the first book dedicated to Grails 3.'

        bookCreatePage.save()

        then:
        browser.at BookShowPage

        when:
        BookShowPage bookShowPage = browser.page BookShowPage

        withConfirm { bookShowPage.buttons.delete() }

        then:
        at BookListPage

        when:
        bookListPage = browser.page BookListPage

        then:
        bookListPage.table.numberOfRows() == 0

        cleanup:
        userRoleDataService.delete(user, role)
        userDataService.delete(user.id)
        roleDataService.delete(role.id)
    }

    def "users without role ROLE_DETECTIVE can view books but not create or delete them"() {
        given:
        User user = userDataService.save('sherlock', 'elementary')

        when:
        via BookListPage

        then:
        at LoginPage

        when:
        LoginPage loginPage = browser.page LoginPage
        loginPage.login(user.username, 'elementary')

        then:
        at BookListPage

        when:
        BookListPage bookListPage = browser.page BookListPage

        then:
        bookListPage.table.numberOfRows() == 0

        when:
        bookListPage.buttons.create()

        then:
        browser.title == 'Denied'

        when:
        Book book = bookDataService.save('Practical Grails 3',
        'Eric Helgeson',
        'Learn the fundamental concepts behind building Grails applications with the first book dedicated to Grails 3.',
        'https://www.grails3book.com/')

        then:
        bookDataService.count() == old(bookDataService.count()) + 1

        when:
        BookShowPage bookShowPage = browser.to BookShowPage, book.id

        then:
        at bookShowPage

        when:
        withConfirm { bookShowPage.buttons.delete() }

        then:
        browser.title == 'Denied'

        cleanup:
        userDataService.delete(user.id)
        bookDataService.delete(book.id)
    }
}
