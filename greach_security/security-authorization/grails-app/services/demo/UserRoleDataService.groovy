package demo

import grails.gorm.services.Service
import groovy.transform.CompileStatic

@CompileStatic
@Service(UserRole)
interface UserRoleDataService {
    UserRole save(User user, Role role)
    void delete(User user, Role role)
}