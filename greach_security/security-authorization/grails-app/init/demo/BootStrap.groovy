package demo

import groovy.transform.CompileStatic

@CompileStatic
class BootStrap {

    UserDataService userDataService

    def init = { servletContext ->
        userDataService.save('watson', 'elementary')
    }

    def destroy = {
    }
}
