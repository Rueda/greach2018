package demo

import grails.plugins.rest.client.RequestCustomizer
import grails.plugins.rest.client.RestBuilder
import grails.plugins.rest.client.RestResponse
import grails.testing.mixin.integration.Integration
import spock.lang.Shared
import spock.lang.Specification

@Integration
class ApiAgendaControllerIntegrationSpec extends Specification {

    @Shared
    RestBuilder rest = new RestBuilder()

    SpeakerGormService speakerGormService
    TalkGormService talkGormService
    TalkSpeakerGormService talkSpeakerGormService
    UserGormService userGormService

    def "if you save a talk, the api returns 201"() {
        given:
        User user = userGormService.save('ilopmar','password', true, false, false, false)

        when:
        RestResponse resp = rest.post("http://localhost:${serverPort}/api/login") {
            accept('application/json')
            contentType('application/json')
            json {
                username = 'ilopmar'
                password = 'password'
            }
        }

        then:
        resp.status == 200

        when:
        resp = saveSpeaker(null,'Sergio del Amo', 'sdelamo', 'I work in the Grails OCI team. Since April 2015, I write Groovy Calamari, a weekly newsletter about the Groovy Ecosystem: Grails, Geb, Gradle, Ratpack, Groovy… I am an experienced web and mobile developer. I like to create products. Understand them, evolve them. I’ve been developing Grails Application for the past six years. I like how succinct and powerful Groovy is. I feel empowered by Grails.')

        then:
        resp.status == 201
        resp.headers['Location'][0].startsWith('/api/speaker/')

        when:
        Long sdelamoId = speakerId(resp)

        then:
        sdelamoId

        when:
        resp = saveTalk(null,'Mapping a Tree with Grails', new Date().parse("dd.MM.yyy HH:mm", '16.03.2018 15:00'), 45, 'One of the most typical data structures to use in any web application is a tree.', [sdelamoId])

        then:
        resp.status == 201
        resp.headers['Location'][0].startsWith('/api/talk/')

        when:
        Long mappingATreeId = talkId(resp)

        then:
        mappingATreeId

        when:
        resp = get('/api') {
            header("Accept-Version", "1.0")
        }

        then:
        resp.status == 200
        resp.json.size() == 1
        resp.json[0].day == '2018-03-16'
        resp.json[0].talks.size() == 1
        resp.json[0].talks[0].title == 'MAPPING A TREE WITH GRAILS'

        cleanup:
        Talk.withTransaction {
            talkSpeakerGormService.delete(Talk.load(mappingATreeId), Speaker.load(sdelamoId))
        }
        talkGormService.delete(mappingATreeId)
        speakerGormService.delete(sdelamoId)
        userGormService.delete(user.id)
    }

    Long speakerId(RestResponse resp) {
        resp.headers['Location'][0].replaceAll('/api/speaker/', '') as Long
    }

    Long talkId(RestResponse resp) {
        resp.headers['Location'][0].replaceAll('/api/talk/', '') as Long
    }


    RestResponse get(String path, @DelegatesTo(RequestCustomizer) Closure customizer = null) {
        rest.get("http://localhost:${serverPort}$path", customizer)
    }

    RestResponse saveSpeaker(String accessToken, String nameParam, String twitterParam, String aboutParam) {
        rest.post("http://localhost:${serverPort}/api/speaker") {
            header('Authorization', "Bearer ${accessToken ?: ''}")
            header("Accept-Version", "1.0")
            accept('application/json')
            contentType('application/json')
            json {
                name = nameParam
                about = aboutParam
                twitter = twitterParam
            }
        }
    }

    RestResponse saveTalk(String accessToken, String titleParam, Date startDateParam, int durationParam, String aboutParam, List<Long> speakersIdParam) {
        rest.post("http://localhost:${serverPort}/api/talk") {
            header('Authorization', "Bearer ${accessToken ?: ''}")
            header("Accept-Version", "1.0")
            accept('application/json')
            contentType('application/json')
            json {
                title = titleParam
                startDate = startDateParam
                duration = durationParam
                about = aboutParam
                speakerIds = speakersIdParam
            }
        }
    }

    RestResponse login(String usernameParma, String passwordParam) {
        rest.post("http://localhost:${serverPort}/api/login") {
            accept('application/json')
            contentType('application/json')
            json {
                username = usernameParma
                password = passwordParam
            }
        }
    }
}
