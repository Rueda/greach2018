package demo

import grails.testing.mixin.integration.Integration
import grails.transaction.Rollback
import org.springframework.beans.factory.annotation.Autowired
import spock.lang.Specification

@Integration
@Rollback
class TalkServiceIntegrationSpec extends Specification {

    @Autowired
    TalkService talkService

    def "findConferenceDays"() {
        given:
        Talk badCode = new Talk(title: 'I\'ve seen Grails code you wouldn\'t believe', startDate: new Date().parse("dd.MM.yyy HH:mm", '16.03.2018 11:15'), duration: 45, about: 'Over the years I’ve created a lot of different Grails applications and I’ve learnt from my mistakes about how to avoid problems and pitfalls for future projects. During more than a year now I’ve also been helping clients to improve their code and develop new features in their applications.')
        badCode.save()

        expect:
        badCode.id

        when:
        TalkDTO talk = talkService.findTalkById(badCode.id)

        then:
        talk
        talk.title == 'I\'ve seen Grails code you wouldn\'t believe'
        talk.duration == 45
        talk.about == 'Over the years I’ve created a lot of different Grails applications and I’ve learnt from my mistakes about how to avoid problems and pitfalls for future projects. During more than a year now I’ve also been helping clients to improve their code and develop new features in their applications.'
    }
}
