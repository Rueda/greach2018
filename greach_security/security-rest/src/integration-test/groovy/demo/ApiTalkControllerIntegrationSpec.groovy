package demo

import grails.plugins.rest.client.RestBuilder
import grails.plugins.rest.client.RestResponse
import grails.testing.mixin.integration.Integration
import spock.lang.Specification

@Integration
class ApiTalkControllerIntegrationSpec extends Specification {

    SpeakerGormService speakerGormService
    TalkGormService talkGormService
    TalkSpeakerGormService talkSpeakerGormService
    UserGormService userGormService

    def "if you save a talk, the api returns 201"() {
        given:
        RestBuilder rest = new RestBuilder()
        User user = userGormService.save('ilopmar','password', true, false, false, false)

        when:
        RestResponse resp = rest.post("http://localhost:${serverPort}/api/login") {
            accept('application/json')
            contentType('application/json')
            json {
                username = 'ilopmar'
                password = 'password'
            }
        }

        then:
        resp.status == 200

        when:
        resp = rest.post("http://localhost:${serverPort}/api/speaker") {
            header("Accept-Version", "1.0")
            accept('application/json')
            contentType('application/json')
            json {
                name = 'Sergio del Amo'
                about = 'I work in the Grails OCI team. Since April 2015, I write Groovy Calamari, a weekly newsletter about the Groovy Ecosystem: Grails, Geb, Gradle, Ratpack, Groovy… I am an experienced web and mobile developer. I like to create products. Understand them, evolve them. I’ve been developing Grails Application for the past six years. I like how succinct and powerful Groovy is. I feel empowered by Grails.'
                twitter = 'sdelamo'
            }
        }

        then:
        resp.status == 201
        resp.headers['Location'][0].startsWith('/api/speaker/')

        when:
        Long speakerId = resp.headers['Location'][0].replaceAll('/api/speaker/', '') as Long

        then:
        speakerId

        when:
        resp = rest.post("http://localhost:${serverPort}/api/talk") {
            header("Accept-Version", "1.0")
            accept('application/json')
            contentType('application/json')
            json {
                title = 'Mapping a Tree with Grails'
                startDate = new Date().parse("dd.MM.yyy HH:mm", '16.03.2018 15:00')
                duration = 45
                about = 'One of the most typical data structures to use in any web application is a tree.'
                speakerIds = [speakerId]
            }
        }

        then:
        resp.status == 201
        resp.headers['Location'][0].startsWith('/api/talk/')

        when:
        Long talkId = resp.headers['Location'][0].replaceAll('/api/talk/', '') as Long

        then:
        talkId

        cleanup:
        Talk.withTransaction {
            talkSpeakerGormService.delete(Talk.load(talkId), Speaker.load(speakerId))
        }
        talkGormService.delete(talkId)
        speakerGormService.delete(speakerId)
        userGormService.delete(user.id)
    }
}
