package demo

import grails.plugins.rest.client.RestBuilder
import grails.plugins.rest.client.RestResponse
import grails.testing.mixin.integration.Integration
import spock.lang.Specification

@Integration
class ApiSpeakerControllerIntegrationSpec extends Specification {

    SpeakerGormService speakerGormService
    UserGormService userGormService
    def "if you save a speaker, the api returns 201"() {
        given:
        RestBuilder rest = new RestBuilder()
        User user = userGormService.save('ilopmar','password', true, false, false, false)

        when:
        RestResponse resp = rest.post("http://localhost:${serverPort}/api/login") {
            accept('application/json')
            contentType('application/json')
            json {
                username = 'ilopmar'
                password = 'password'
            }
        }

        then:
        resp.status == 200

        when:
        resp = rest.post("http://localhost:${serverPort}/api/speaker") {
            header("Accept-Version", "1.0")
            accept('application/json')
            contentType('application/json')
            json {
                name = 'Sergio del Amo'
                about = 'I work in the Grails OCI team. Since April 2015, I write Groovy Calamari, a weekly newsletter about the Groovy Ecosystem: Grails, Geb, Gradle, Ratpack, Groovy… I am an experienced web and mobile developer. I like to create products. Understand them, evolve them. I’ve been developing Grails Application for the past six years. I like how succinct and powerful Groovy is. I feel empowered by Grails.'
                twitter = 'sdelamo'
            }
        }

        then:
        resp.status == 201
        resp.headers['Location'][0].startsWith('/api/speaker/')

        when:
        Long speakerId = resp.headers['Location'][0].replaceAll('/api/speaker/', '') as Long

        then:
        speakerId

        cleanup:
        speakerGormService.delete(speakerId)
        userGormService.delete(user.id)
    }

}
