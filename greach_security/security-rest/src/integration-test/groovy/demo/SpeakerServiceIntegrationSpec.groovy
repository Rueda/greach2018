package demo

import grails.testing.mixin.integration.Integration
import grails.transaction.Rollback
import org.springframework.beans.factory.annotation.Autowired
import spock.lang.Specification

@Integration
@Rollback
class SpeakerServiceIntegrationSpec extends Specification {

    @Autowired
    SpeakerService speakerService

    def "findConferenceDays"() {
        given:
        Speaker ilopmar = new Speaker(name: 'Iván López', about: 'Iván is a Software Engineer and Systems Administrator with 14 years of experience. He is a member of the Grails team at OCI. He discovered Grails 7 years ago and since then he develops almost exclusively using Groovy. He is the creator of some Grails plugins like Postgresql-Extensions and Slug-Generator.', twitter: 'ilopmar')
        ilopmar.save()

        expect:
        ilopmar.id

        when:
        SpeakerDTO speaker = speakerService.findSpeakerById(ilopmar.id)

        then:
        speaker
        speaker.name == 'Iván López'
        speaker.twitter == 'ilopmar'
        speaker.about == 'Iván is a Software Engineer and Systems Administrator with 14 years of experience. He is a member of the Grails team at OCI. He discovered Grails 7 years ago and since then he develops almost exclusively using Groovy. He is the creator of some Grails plugins like Postgresql-Extensions and Slug-Generator.'
    }
}
