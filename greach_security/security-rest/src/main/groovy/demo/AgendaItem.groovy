package demo

import groovy.transform.CompileStatic
import groovy.transform.ToString

@ToString
@CompileStatic
class AgendaItem {
    TalkDTO talk
    Set<IdName> speakers
}
