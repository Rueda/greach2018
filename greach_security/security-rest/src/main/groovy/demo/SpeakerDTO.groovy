package demo

import groovy.transform.CompileStatic

@CompileStatic
class SpeakerDTO {
    Long id
    String name
    String about
    String twitter

    static SpeakerDTO of(Speaker speaker) {
        if ( !speaker ) {
            return null
        }
        SpeakerDTO dto = new SpeakerDTO()
        dto.with {
            id = speaker.id
            name = speaker.name
            about = speaker.about
            twitter = speaker.twitter
        }
        dto
    }
}
