package demo

import groovy.transform.CompileStatic
import groovy.transform.ToString

import java.time.LocalDate

@ToString
@CompileStatic
class ConferenceDay {
    LocalDate day
    Set<AgendaItem> items
}
