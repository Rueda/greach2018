package demo

import groovy.transform.CompileStatic
import groovy.transform.ToString

@ToString
@CompileStatic
class IdName {
    Long id
    String name

    static IdName of(Speaker speaker) {
        if ( !speaker ) {
            return null
        }
        new IdName(id: speaker.id, name: speaker.name)
    }

    static IdName of(Talk talk) {
        if ( !talk ) {
            return null
        }
        new IdName(id: talk.id, name: talk.title)
    }
}
