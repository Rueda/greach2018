package demo

import groovy.transform.CompileStatic

@CompileStatic
class TalkDTO {
    Long id
    String title
    String about
    Date startDate
    int duration

    static TalkDTO of(Talk talk) {
        if ( !talk ) {
            return null
        }
        TalkDTO dto = new TalkDTO()
        dto.with {
            id = talk.id
            title = talk.title
            about = talk.about
            startDate = talk.startDate
            duration = talk.duration
        }
        dto
    }
}
