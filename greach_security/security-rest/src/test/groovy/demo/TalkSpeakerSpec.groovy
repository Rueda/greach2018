package demo

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification

class TalkSpeakerSpec extends Specification implements DomainUnitTest<TalkSpeaker> {

    def "speaker cannot be null"() {
        when:
        domain.speaker = null

        then:
        !domain.validate(['speaker'])
    }

    def "talk cannot be null"() {
        when:
        domain.talk = null

        then:
        !domain.validate(['talk'])
    }
}
