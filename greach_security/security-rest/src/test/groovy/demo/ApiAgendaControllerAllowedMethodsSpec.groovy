package demo

import grails.testing.web.controllers.ControllerUnitTest
import spock.lang.Specification
import spock.lang.Unroll

import static javax.servlet.http.HttpServletResponse.SC_METHOD_NOT_ALLOWED
import static javax.servlet.http.HttpServletResponse.SC_OK

class ApiAgendaControllerAllowedMethodsSpec extends Specification
        implements ControllerUnitTest<ApiAgendaController> {

    @Unroll
    def "test AgendaController.index does not accept #method requests"(String method) {
        when:
        request.method = method
        controller.index()

        then:
        response.status == SC_METHOD_NOT_ALLOWED

        where:
        method << ['PATCH', 'DELETE', 'POST', 'PUT']
    }

    def "test AgendaController.index accepts POST requests"() {
        given:
        controller.agendaService = Mock(AgendaService)

        when:
        request.method = 'GET'
        controller.index()

        then:
        response.status == SC_OK
    }
}