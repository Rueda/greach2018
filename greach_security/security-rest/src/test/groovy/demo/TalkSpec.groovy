package demo

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification

class TalkSpec extends Specification implements DomainUnitTest<Talk> {

    def "title cannot be null"() {
        when:
        domain.title = null

        then:
        !domain.validate(['title'])
    }

    void 'test title can have a maximum of 255 characters'() {
        when: 'for a string of 256 characters'
        String str = 'a' * 256
        domain.title = str

        then: 'title validation fails'
        !domain.validate(['title'])
        domain.errors['title'].code == 'maxSize.exceeded'

        when: 'for a string of 256 characters'
        str = 'a' * 255
        domain.title = str

        then: 'title validation passes'
        domain.validate(['title'])
    }

    def "about can have more than 255 characters"() {
        when: 'for a string of 256 characters'
        String str = 'a' * 256
        domain.about = str

        then: 'about validation passes'
        domain.validate(['about'])
    }

    def "about cannot be null"() {
        when:
        domain.about = null

        then:
        !domain.validate(['about'])
    }
}
