package demo

import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

class SaveSpeakerCommandSpec extends Specification {

    @Subject
    @Shared
    SaveSpeakerCommand cmd = new SaveSpeakerCommand()

    def "name cannot be null"() {
        when:
        cmd.name = null

        then:
        !cmd.validate(['name'])
    }

    void 'test name can have a maximum of 255 characters'() {
        when: 'for a string of 256 characters'
        String str = 'a' * 256
        cmd.name = str

        then: 'name validation fails'
        !cmd.validate(['name'])
        cmd.errors['name'].code == 'maxSize.exceeded'

        when: 'for a string of 256 characters'
        str = 'a' * 255
        cmd.name = str

        then: 'name validation passes'
        cmd.validate(['name'])
    }

    def "about can have more than 255 characters"() {
        when: 'for a string of 256 characters'
        String str = 'a' * 256
        cmd.about = str

        then: 'about validation passes'
        cmd.validate(['about'])
    }

    def "about cannot be null"() {
        when:
        cmd.about = null

        then:
        !cmd.validate(['about'])
    }

    def "twitter can be null"() {
        when:
        cmd.twitter = null

        then:
        cmd.validate(['twitter'])
    }
}
