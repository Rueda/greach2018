package demo

import grails.testing.web.controllers.ControllerUnitTest
import spock.lang.Specification

import static javax.servlet.http.HttpServletResponse.SC_OK

class ApiSpeakerControllerSpec extends Specification
implements ControllerUnitTest<ApiSpeakerController> {

    def "test SpeakerController.show model contains speaker"() {
        given:
        controller.speakerService = Mock(SpeakerService)

        when:
        request.method = 'GET'
        Map m = controller.show()

        then:
        response.status == SC_OK
        m.keySet().contains('speaker')
        1 *  controller.speakerService.findSpeakerById(_)
    }
}
