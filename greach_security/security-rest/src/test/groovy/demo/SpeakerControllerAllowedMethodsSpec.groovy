package demo

import grails.testing.web.controllers.ControllerUnitTest
import spock.lang.Specification
import spock.lang.Unroll

import static javax.servlet.http.HttpServletResponse.SC_METHOD_NOT_ALLOWED
import static javax.servlet.http.HttpServletResponse.SC_OK

class SpeakerControllerAllowedMethodsSpec extends Specification
        implements ControllerUnitTest<SpeakerController> {

    @Unroll
    def "test SpeakerController.show does not accept #method requests"(String method) {
        when:
        request.method = method
        controller.show()

        then:
        response.status == SC_METHOD_NOT_ALLOWED

        where:
        method << ['PATCH', 'DELETE', 'POST', 'PUT']
    }

    def "test SpeakerController.show accepts POST requests"() {
        given:
        controller.speakerService = Mock(SpeakerService)

        when:
        request.method = 'GET'
        controller.show()

        then:
        response.status == SC_OK
    }
}
