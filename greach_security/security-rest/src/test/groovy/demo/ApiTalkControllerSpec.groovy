package demo

import grails.testing.web.controllers.ControllerUnitTest
import spock.lang.Specification

import static javax.servlet.http.HttpServletResponse.SC_OK

class ApiTalkControllerSpec  extends Specification
        implements ControllerUnitTest<ApiTalkController> {

    def "test TalkController.show model contains talk"() {
        given:
        controller.talkService = Mock(TalkService)

        when:
        request.method = 'GET'
        Map m = controller.show()

        then:
        response.status == SC_OK

        and:
        m.keySet().contains('talk')

        and:
        1 *  controller.talkService.findTalkById(_)
    }
}
