package demo

import grails.testing.web.controllers.ControllerUnitTest
import spock.lang.Specification

import static javax.servlet.http.HttpServletResponse.SC_OK

class AgendaControllerSpec extends Specification implements ControllerUnitTest<AgendaController> {

    def "test AgendaController.index model contains days"() {
        given:
        controller.agendaService = Mock(AgendaService)

        when:
        request.method = 'GET'
        Map m = controller.index()

        then:
        response.status == SC_OK

        and:
        m.keySet().contains('days')

        and:
        1 *  controller.agendaService.findConferenceDays()
    }

}
