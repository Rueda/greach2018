package demo

import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

class SaveTalkCommandSpec extends Specification {

    @Subject
    @Shared
    SaveTalkCommand cmd = new SaveTalkCommand()

    def "title cannot be null"() {
        when:
        cmd.title = null

        then:
        !cmd.validate(['title'])
    }

    def "speakerIds cannot be null"() {
        when:
        cmd.speakerIds = null

        then:
        !cmd.validate(['speakerIds'])
    }

    def "speakerIds cannot be an empty list"() {
        when:
        cmd.speakerIds = []

        then:
        !cmd.validate(['speakerIds'])
    }

    void 'test title can have a maximum of 255 characters'() {
        when: 'for a string of 256 characters'
        String str = 'a' * 256
        cmd.title = str

        then: 'title validation fails'
        !cmd.validate(['title'])
        cmd.errors['title'].code == 'maxSize.exceeded'

        when: 'for a string of 256 characters'
        str = 'a' * 255
        cmd.title = str

        then: 'title validation passes'
        cmd.validate(['title'])
    }

    def "about can have more than 255 characters"() {
        when: 'for a string of 256 characters'
        String str = 'a' * 256
        cmd.about = str

        then: 'about validation passes'
        cmd.validate(['about'])
    }

    def "about cannot be null"() {
        when:
        cmd.about = null

        then:
        !cmd.validate(['about'])
    }
}
