package demo

import grails.testing.web.controllers.ControllerUnitTest
import spock.lang.Specification
import spock.lang.Unroll

import static javax.servlet.http.HttpServletResponse.SC_METHOD_NOT_ALLOWED
import static javax.servlet.http.HttpServletResponse.SC_OK

class TalkControllerAllowedMethodsSpec extends Specification
        implements ControllerUnitTest<TalkController> {

    @Unroll
    def "test TalkController.show does not accept #method requests"(String method) {
        when:
        request.method = method
        controller.show()

        then:
        response.status == SC_METHOD_NOT_ALLOWED

        where:
        method << ['PATCH', 'DELETE', 'POST', 'PUT']
    }

    def "test TalkController.show accepts POST requests"() {
        given:
        controller.talkService = Mock(TalkService)

        when:
        request.method = 'GET'
        controller.show()

        then:
        response.status == SC_OK
    }
}
