package demo

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification

class SpeakerSpec extends Specification implements DomainUnitTest<Speaker> {

    def "name cannot be null"() {
        when:
        domain.name = null

        then:
        !domain.validate(['name'])
    }

    void 'test name can have a maximum of 255 characters'() {
        when: 'for a string of 256 characters'
        String str = 'a' * 256
        domain.name = str

        then: 'name validation fails'
        !domain.validate(['name'])
        domain.errors['name'].code == 'maxSize.exceeded'

        when: 'for a string of 256 characters'
        str = 'a' * 255
        domain.name = str

        then: 'name validation passes'
        domain.validate(['name'])
    }

    def "about can have more than 255 characters"() {
        when: 'for a string of 256 characters'
        String str = 'a' * 256
        domain.about = str

        then: 'about validation passes'
        domain.validate(['about'])
    }

    def "about cannot be null"() {
        when:
        domain.about = null

        then:
        !domain.validate(['about'])
    }

    def "twitter can be null"() {
        when:
        domain.twitter = null

        then:
        domain.validate(['twitter'])
    }
}
