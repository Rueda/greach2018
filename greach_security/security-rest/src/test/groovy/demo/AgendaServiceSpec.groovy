package demo

import grails.testing.services.ServiceUnitTest
import spock.lang.Specification

class AgendaServiceSpec extends Specification implements ServiceUnitTest<AgendaService> {

    def "findConferenceDays"() {
        given:

        Speaker ilopmar = new Speaker(name: 'Iván López', about: 'Iván is a Software Engineer and Systems Administrator with 14 years of experience. He is a member of the Grails team at OCI. He discovered Grails 7 years ago and since then he develops almost exclusively using Groovy. He is the creator of some Grails plugins like Postgresql-Extensions and Slug-Generator.', twitter: 'ilopmar')

        Talk badCode = new Talk(title: 'I\'ve seen Grails code you wouldn\'t believe', startDate: new Date().parse("dd.MM.yyy HH:mm", '16.03.2018 11:15'), duration: 45, about: 'Over the years I’ve created a lot of different Grails applications and I’ve learnt from my mistakes about how to avoid problems and pitfalls for future projects. During more than a year now I’ve also been helping clients to improve their code and develop new features in their applications.')

        TalkSpeaker ilopmarBadCode = new TalkSpeaker(talk: badCode, speaker: ilopmar)

        Speaker sdelamo = new Speaker(name: 'Sergio del Amo', about: 'I work in the Grails OCI team. Since April 2015, I write Groovy Calamari, a weekly newsletter about the Groovy Ecosystem: Grails, Geb, Gradle, Ratpack, Groovy… I am an experienced web and mobile developer. I like to create products. Understand them, evolve them. I’ve been developing Grails Application for the past six years. I like how succinct and powerful Groovy is. I feel empowered by Grails.', twitter: 'sdelamo')

        Talk mappingATree = new Talk(title: 'Mapping a Tree with Grails', startDate:  new Date().parse("dd.MM.yyy HH:mm", '16.03.2018 15:00'), duration: 45, about: 'One of the most typical data structures to use in any web application is a tree.')

        TalkSpeaker mappingATreeSdelamo = new TalkSpeaker(talk: mappingATree, speaker: sdelamo)

        Talk grailsSecurity = new Talk(title:'Workshop: Grails Security', startDate: new Date().parse("dd.MM.yyy HH:mm", '15.03.2018 09:00'), duration: 180, about: 'This workshop involves lecture, sample projects, and hands-on lab exercises. It provides an overview of the Grails Security Plugin Landscape.')

        TalkSpeaker grailsSecuritySdelamo = new TalkSpeaker(talk: grailsSecurity, speaker: sdelamo)

        Speaker jorge = new Speaker(name: 'Miguel Angel Rueda', about:  'I am currently part of the company Feu Vert in which I have been working as a support and developer in java since 2010. ')
        Speaker miguel = new Speaker(name: 'Jorge Aguilera', about:  'More than 8 years working with groovy and grails in every project we have opportunity (and we sum more years working as IT that some of your audience walking on the earth)')
        Talk scriptsTalk =  new Talk(title: 'Lightning Talk: 101 scripts that can save you the day', startDate:  new Date().parse("dd.MM.yyy HH:mm", '17.03.2018 11:45'), duration:  15, about: 'We are collecting and documenting as many groovy scripts as we can imagine that can be useful in a typical day. In this talk we wan to show you some of them and who/why/how we do it.')

        TalkSpeaker scriptsTalkJorge = new TalkSpeaker(talk: scriptsTalk, speaker: jorge)
        TalkSpeaker scriptsTalkMiguel = new TalkSpeaker(talk: scriptsTalk, speaker: miguel)

        service.talkSpeakerGormService = Stub(TalkSpeakerGormService) {
            findAll() >> [ilopmarBadCode, mappingATreeSdelamo, grailsSecuritySdelamo, scriptsTalkJorge, scriptsTalkMiguel]
        }

        when:
        List<ConferenceDay> days = service.findConferenceDays()

        then:
        days
        days.size() == 3

        and:
        days[0].day.toString() == '2018-03-15'

        days[0].items*.talk.title == ['Workshop: Grails Security']
        days[0].items.speakers*.name.flatten() == ['Sergio del Amo']

        and:
        days[1].day.toString() == '2018-03-16'
        days[1].items*.talk.title == ['I\'ve seen Grails code you wouldn\'t believe', 'Mapping a Tree with Grails']
        days[1].items.speakers*.name.flatten() == ['Iván López', 'Sergio del Amo']

        and:
        days
        days[2].day.toString() == '2018-03-17'
        days[2].items*.talk.title == ['Lightning Talk: 101 scripts that can save you the day']
        days[2].items.speakers*.name.flatten() == ['Miguel Angel Rueda', 'Jorge Aguilera']
    }

}
