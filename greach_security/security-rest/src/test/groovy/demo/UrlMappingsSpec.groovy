package demo

import grails.testing.web.UrlMappingsUnitTest
import spock.lang.Specification

class UrlMappingsSpec extends Specification implements UrlMappingsUnitTest<UrlMappings> {

    void setup() {
        mockController(ApiSpeakerController)
        mockController(ApiAgendaController)
        mockController(ApiTalkController)
    }

    void "verify /api maps to agenda controller"() {
        expect:
        verifyForwardUrlMapping("/api", controller: 'apiAgenda', action: 'index')
    }

    void "verify /api/speaker/id maps to speaker controller"() {
        expect:
        verifyForwardUrlMapping("/api/speaker/123", controller: 'apiSpeaker', action: 'show') {
            id = '123'
        }
    }

    void "verify /api/talk/id maps to talk controller"() {
        expect:
        verifyForwardUrlMapping("/api/talk/123", controller: 'apiTalk', action: 'show') {
            id = '123'
        }
    }
}
