package demo

import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
class TalkSpeaker {
    Speaker speaker
    Talk talk

    static constraints = {
        speaker nullable: false
        talk nullable: false
    }
}