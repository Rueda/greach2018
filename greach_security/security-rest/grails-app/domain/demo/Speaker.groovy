package demo

import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
class Speaker {
    String name
    String twitter
    String about

    static constraints = {
        name nullable: false, maxSize: 255
        twitter nullable: true
        about nullable: false
    }

    static mapping = {
        about type: 'text'
    }
}