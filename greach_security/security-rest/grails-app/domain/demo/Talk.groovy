package demo

import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
class Talk {
    String title
    String about
    Date startDate
    int duration

    static constraints = {
        title nullable: false, maxSize: 255
        about nullable: false
        startDate nullable: false
        duration min: 0
    }

    static mapping = {
        about type: 'text'
    }
}