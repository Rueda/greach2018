package demo

import grails.plugin.springsecurity.annotation.Secured
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j

@Secured('isAuthenticated()')
@Slf4j
@CompileStatic
class SpeakerController {
    static allowedMethods = [show: 'GET']

    SpeakerService speakerService

    def show(Long id) {
        [speaker: speakerService.findSpeakerById(id)]
    }

    def save(SaveSpeakerCommand cmd) {
        if ( cmd.hasErrors() ) {
            render status: 422
            return
        }
        SpeakerDTO speaker = speakerService.save(cmd)
        if ( !speaker ) {
            render status: 422
            return
        }
        [speaker: speaker]
    }
}