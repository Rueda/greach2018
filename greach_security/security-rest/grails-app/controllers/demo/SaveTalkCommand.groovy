package demo

import grails.compiler.GrailsCompileStatic
import grails.validation.Validateable

@GrailsCompileStatic
class SaveTalkCommand implements Validateable {
    String title
    String about
    Date startDate
    int duration
    List<Long> speakerIds

    static constraints = {
        title nullable: false, maxSize: 255
        about nullable: false
        speakerIds nullable: false, minSize: 1
        startDate nullable: false
        duration min: 0
    }
}