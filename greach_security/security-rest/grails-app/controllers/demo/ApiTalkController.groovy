package demo

import grails.plugin.springsecurity.annotation.Secured
import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic

@Secured('isAuthenticated()')
@CompileStatic
class ApiTalkController {
    static namespace = 'v1'

    static allowedMethods = [
            show: 'GET',
            save: 'POST'
    ]

    TalkService talkService

    def show(Long id) {
        [talk: talkService.findTalkById(id)]
    }

    def save(SaveTalkCommand cmd) {
        if ( cmd.hasErrors() ) {
            render status: 422
            return
        }
        TalkDTO talk = talkService.save(cmd)
        if ( !talk ) {
            render status: 422
            return
        }
        response.addHeader('Location', locationUrl(talk))
        render status: 201
    }

    @CompileDynamic
    protected String locationUrl(TalkDTO talk) {
        g.createLink(uri: "/api/talk/${talk.id}")
    }

}