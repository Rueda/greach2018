package demo

class UrlMappings {

    static mappings = {
        "/api/talk/$id"(controller: 'apiTalk', action: 'show', method: "GET", version:'1.0', namespace:'v1')
        "/api/speaker/$id"(controller: 'apiSpeaker', action: 'show', method: "GET", version:'1.0', namespace:'v1')
        "/api/speaker"(controller: 'apiSpeaker', action: 'save', method: "POST", version:'1.0', namespace:'v1')
        "/api/talk"(controller: 'apiTalk', action: 'save', method: "POST", version:'1.0', namespace:'v1')
        "/api"(controller: 'apiAgenda', method: 'GET', version:'1.0', namespace:'v1')
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
