package demo

import grails.compiler.GrailsCompileStatic
import grails.validation.Validateable

@GrailsCompileStatic
class SaveSpeakerCommand implements Validateable {
    String name
    String about
    String twitter

    static constraints = {
        name nullable: false, maxSize: 255
        about nullable: false
        twitter nullable: true
    }
}