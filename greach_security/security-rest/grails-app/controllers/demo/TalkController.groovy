package demo

import grails.plugin.springsecurity.annotation.Secured
import groovy.transform.CompileStatic

@Secured('isAuthenticated()')
@CompileStatic
class TalkController {
    static allowedMethods = [show: 'GET']

    TalkService talkService

    def show(Long id) {
        [talk: talkService.findTalkById(id)]
    }

    def save(SaveTalkCommand cmd) {
        if ( cmd.hasErrors() ) {
            render status: 422
            return
        }
        TalkDTO talk = talkService.save(cmd)
        if ( !talk ) {
            render status: 422
            return
        }
        [speaker: talk]
    }
}