package demo

import grails.plugin.springsecurity.annotation.Secured
import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j

@Secured('isAuthenticated()')
@Slf4j
@CompileStatic
class ApiSpeakerController {
    static namespace = 'v1'

    static allowedMethods = [
            show: 'GET',
            save: 'POST'
    ]

    SpeakerService speakerService

    def show(Long id) {
        [speaker: speakerService.findSpeakerById(id)]
    }

    def save(SaveSpeakerCommand cmd) {
        if ( cmd.hasErrors() ) {
            render status: 422
            return
        }
        SpeakerDTO speaker = speakerService.save(cmd)
        if ( !speaker ) {
            render status: 422
            return
        }
        response.addHeader('Location', locationUrl(speaker))
        render status: 201
    }

    @CompileDynamic
    protected String locationUrl(SpeakerDTO speaker) {
        g.createLink(uri: "/api/speaker/${speaker.id}")
    }

}