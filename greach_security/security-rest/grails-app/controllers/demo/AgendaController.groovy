package demo

import grails.plugin.springsecurity.annotation.Secured
import groovy.transform.CompileStatic

@Secured('isAuthenticated()')
@CompileStatic
class AgendaController {

    static allowedMethods = [index: 'GET']

    AgendaService agendaService

    def index() {
        [days: agendaService.findConferenceDays()]
    }
}