package demo

import groovy.transform.CompileStatic

@CompileStatic
class SpeakerService {

    SpeakerGormService speakerGormService

    SpeakerDTO findSpeakerById(Long id) {
        SpeakerDTO.of(speakerGormService.findById(id))
    }

    SpeakerDTO save(SaveSpeakerCommand cmd) {
        SpeakerDTO.of(speakerGormService.save(cmd.name, cmd.twitter, cmd.about))
    }
}