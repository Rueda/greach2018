package demo

import grails.gorm.transactions.Transactional
import groovy.transform.CompileStatic

@CompileStatic
class TalkService {

    TalkGormService talkGormService
    TalkSpeakerGormService talkSpeakerGormService

    TalkDTO findTalkById(Long id) {
        TalkDTO.of(talkGormService.findById(id))
    }

    @Transactional
    TalkDTO save(SaveTalkCommand cmd) {
        Talk talk = talkGormService.save(cmd.title, cmd.about, cmd.startDate, cmd.duration)
        for ( Long speakerId : cmd.speakerIds ) {
            talkSpeakerGormService.save(talk, Speaker.load(speakerId))
        }
        TalkDTO.of(talk)
    }
}