package demo

import groovy.transform.CompileStatic
import java.time.LocalDate

@CompileStatic
class AgendaService {

    TalkSpeakerGormService talkSpeakerGormService

    List<ConferenceDay> findConferenceDays() {
        List<TalkSpeaker> talkSpeakerList =  talkSpeakerGormService.findAll()
        if ( !talkSpeakerList ) {
            return [] as List<ConferenceDay>
        }
        List<ConferenceDay> conferenceDayList = []
        for ( LocalDate day : daysAtTalkSpeakerList(talkSpeakerList) ) {
            conferenceDayList << conferenceDay(day, talkSpeakerList)
        }
        conferenceDayList
    }

    ConferenceDay conferenceDay(LocalDate day, List<TalkSpeaker> talkSpeakerList) {
        Set<Talk> talks = talksByDay(day, talkSpeakerList)
        new ConferenceDay(day: day, items: talks.collect { Talk talk ->
            new AgendaItem(talk: TalkDTO.of(talk), speakers: speakersByTalk(talk, talkSpeakerList))
        } as Set<AgendaItem>)
    }

    Set<Talk> talksByDay(LocalDate day, List<TalkSpeaker> talkSpeakerList) {
        talkSpeakerList.findAll { TalkSpeaker talkSpeaker ->
            localDateOfTalkSpeaker(talkSpeaker) == day
        }.collect { TalkSpeaker talkSpeaker ->
            talkSpeaker.talk
        } as Set<Talk>
    }

    Set<IdName> speakersByTalk(Talk talk, List<TalkSpeaker> talkSpeakerList) {
        talkSpeakerList.findAll { it.talk == talk }.collect { TalkSpeaker talkSpeaker ->
            IdName.of(talkSpeaker.speaker)
        } as Set<IdName>
    }

    SortedSet<LocalDate> daysAtTalkSpeakerList(List<TalkSpeaker> talkSpeakerList) {
        (talkSpeakerList.collect { TalkSpeaker talkSpeaker ->
            localDateOfTalkSpeaker(talkSpeaker)
        } as SortedSet<LocalDate>).sort { LocalDate a, LocalDate b -> b <=> a } as SortedSet<LocalDate>
    }

    LocalDate localDateOfTalkSpeaker(TalkSpeaker talkSpeaker) {
        LocalDate.of(1900 + talkSpeaker.talk.startDate.year, (1 + talkSpeaker.talk.startDate.month), talkSpeaker.talk.startDate.date, )
    }
}