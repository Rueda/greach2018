package demo

import grails.gorm.services.Service

@Service(Role)
interface RoleGormService {
    Role save(String authority)
    void delete(Serializable id)
}