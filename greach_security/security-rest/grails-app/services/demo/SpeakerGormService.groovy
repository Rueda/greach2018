package demo

import grails.gorm.services.Service

@Service(Speaker)
interface SpeakerGormService {
    Speaker findById(Long id)
    Speaker save(String name, String twitter, String about)
    void delete(Serializable id)
}