package demo

import grails.gorm.services.Service

@Service(UserRole)
interface UserRoleGormService {
    UserRole save(User user, Role role)
    void delete(Serializable id)
}