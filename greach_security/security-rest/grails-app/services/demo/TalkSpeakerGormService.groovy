package demo

import grails.gorm.services.Service

@Service(TalkSpeaker)
interface TalkSpeakerGormService {
    List<TalkSpeaker> findAll()
    TalkSpeaker save(Talk talk, Speaker speaker)
    void delete(Talk talk, Speaker speaker)
}