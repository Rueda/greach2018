package demo

import grails.gorm.services.Service

@Service(Talk)
interface TalkGormService {
    Talk findById(Long id)
    Talk save(String title, String about, Date startDate, int duration)
    void delete(Serializable id)
}