package demo

import geb.spock.GebSpec
import grails.testing.mixin.integration.Integration

@Integration
class BookListPageSecuredSpec extends GebSpec {

    UserDataService userDataService

    def "book list page is secured"() {
        given:
        User user = userDataService.save('sherlock', 'elementary')

        when:
        via BookListPage

        then:
        at LoginPage

        when:
        LoginPage loginPage = browser.page LoginPage
        loginPage.login(user.username, 'elementary')

        then:
        at BookListPage

        cleanup:
        userDataService.delete(user.id)
    }
}
