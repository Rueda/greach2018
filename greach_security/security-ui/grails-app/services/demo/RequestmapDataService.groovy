package demo

import grails.gorm.services.Service
import org.springframework.http.HttpMethod

@Service(Requestmap)
interface RequestmapDataService {
    Requestmap save(String url, String configAttribute, HttpMethod httpMethod)
    Requestmap save(String url, String configAttribute)
}