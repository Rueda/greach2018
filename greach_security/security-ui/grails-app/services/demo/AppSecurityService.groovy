package demo

import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import groovy.transform.CompileStatic
import org.springframework.http.HttpMethod

@CompileStatic
class AppSecurityService {

    RequestmapDataService requestmapDataService
    SpringSecurityService springSecurityService

    @Transactional
    void init() {
        secureUrls([
                '/', '/error', '/index', '/index.gsp', '/**/favicon.ico', '/shutdown',
                '/assets/**', '/**/js/**', '/**/css/**', '/**/images/**',
                '/login', '/login.*', '/login/*',
                '/logout', '/logout.*', '/logout/*'], 'permitAll')

        secureUrls(['/book/index', '/book/create', 'book/delete/*', '/book/show/*', '/book/edit/*', '/book', '/book/save', '/book/delete'], 'isAuthenticated()')

        secureUrls(['/user/**',
            '/userAdmin/**',
            '/role/**',
            '/aclClass/**',
            '/aclSid/**',
            '/aclEntry/**',
            '/aclObjectIdentity/**',
            '/requestMap/**',
            '/registrationCode/**',
            '/securityInfo/**',], 'isAuthenticated()')
    }

    @Transactional
    void secureUrls(List<String> urls, String configAttribute = 'isAuthenticated()', HttpMethod httpMethod = null) {
        for (String url in urls) {
            if ( httpMethod ) {
                requestmapDataService.save(url, configAttribute, httpMethod)
            } else {
                requestmapDataService.save(url, configAttribute)
            }
        }
        springSecurityService.clearCachedRequestmaps()
    }
}