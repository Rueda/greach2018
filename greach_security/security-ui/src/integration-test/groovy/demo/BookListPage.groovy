package demo

import geb.Page

class BookListPage extends Page {

    static url = '/book/index'

    static at = { title == 'Book List' }
}
